﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorControl : MonoBehaviour
{
    private Renderer render;

    public Material NoColor;
    public Material Player1Color;
    public Material Player2Color;
    void Start()
    {
        render = GetComponent<Renderer>();
        render.material = NoColor;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player1") && gameObject.tag != "Blue" || other.gameObject.CompareTag("Bullet1") && gameObject.tag != "Blue")
        {
            gameObject.tag = "Blue";
            render.material = Player1Color;
        }

        if (other.gameObject.CompareTag("Player2") && gameObject.tag != "Red" || other.gameObject.CompareTag("Bullet2") && gameObject.tag != "Red")
        {
            gameObject.tag = "Red";
            render.material = Player2Color;
        }
    }
}
