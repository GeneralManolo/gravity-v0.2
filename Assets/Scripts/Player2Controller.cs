﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Controller : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    private Rigidbody rb;
    public bool fly = false;
    public float jumpForce;

    public GameObject[] planets;

    public GameObject Cannon;
    public GameObject Bullet;
    public int firePower;

    public int life;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //search for the closest planet
        float closeDistance = Vector3.Distance(transform.position, planets[0].transform.position);
        int closePlanet = 0;

        for (int i = 1; i < planets.Length; i++)
        {
            float distAux = Vector3.Distance(transform.position, planets[i].transform.position);
            if (distAux < closeDistance)
            {
                closeDistance = distAux;
                closePlanet = i;
            }
        }

        //Attracts the player to the planet
        Physics.gravity = planets[closePlanet].transform.position - transform.position;

        //Makes the player's feet to stay on the planet
        transform.rotation = Quaternion.FromToRotation(transform.up, -Physics.gravity) * transform.rotation;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(new Vector3(0, -rotationSpeed * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(new Vector3(0, rotationSpeed * Time.deltaTime, 0));
        }

        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            GameObject bullet = Instantiate(Bullet);
            Rigidbody rbBullet = bullet.GetComponent<Rigidbody>();

            bullet.transform.position = Cannon.transform.position;
            rbBullet.AddForce(Cannon.transform.forward * firePower, ForceMode.Impulse);

            Destroy(bullet, 5);
        }

        if (!fly)
        {
            if (Input.GetKey(KeyCode.RightControl))
            {
                rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
                fly = true;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        fly = false;

        if (collision.gameObject.CompareTag("Bullet2"))
        {
            --life;
        }
    }
}
