﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnController : MonoBehaviour
{
    public GameObject Spawn1;
    public GameObject Spawn2;

    public PlayerController player1;
    public Player2Controller player2;
    void Start()
    {
        player1 = GameObject.FindObjectOfType<PlayerController>();
        player2 = GameObject.FindObjectOfType<Player2Controller>();
    }

    void Update()
    {
        if (player1.life == 0)
        {
            player1.transform.position = Spawn1.transform.position;
            player1.life = 3;
        }

        if (player2.life == 0)
        {
            player2.transform.position = Spawn2.transform.position;
            player2.life = 3;
        }
    }
}
